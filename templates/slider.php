<?php
  
  $show_slide1 = theme_get_setting('show_slide1');
  $slide1_head = check_plain(theme_get_setting('slide1_head'));
  $slide1_desc = filter_xss_admin(theme_get_setting('slide1_desc'));
  $slide1_url = check_plain(theme_get_setting('slide1_url'));
  $slide1_img = check_plain(theme_get_setting('slide1_image_url'));
  $slide1_alt = check_plain(theme_get_setting('slide1_alt'));

  $show_slide2 = theme_get_setting('show_slide2');
  $slide2_head = check_plain(theme_get_setting('slide2_head'));
  $slide2_desc = filter_xss_admin(theme_get_setting('slide2_desc'));
  $slide2_url = check_plain(theme_get_setting('slide2_url'));
  $slide2_img = check_plain(theme_get_setting('slide2_image_url'));
  $slide2_alt = check_plain(theme_get_setting('slide2_alt'));

  $show_slide3 = theme_get_setting('show_slide3');
  $slide3_head = check_plain(theme_get_setting('slide3_head'));
  $slide3_desc = filter_xss_admin(theme_get_setting('slide3_desc'));
  $slide3_url = check_plain(theme_get_setting('slide3_url'));
  $slide3_img = check_plain(theme_get_setting('slide3_image_url'));
  $slide3_alt = check_plain(theme_get_setting('slide3_alt'));

  $show_slide4 = theme_get_setting('show_slide4');
  $slide4_head = check_plain(theme_get_setting('slide4_head'));
  $slide4_desc = filter_xss_admin(theme_get_setting('slide4_desc'));
  $slide4_url = check_plain(theme_get_setting('slide4_url'));
  $slide4_img = check_plain(theme_get_setting('slide4_image_url'));
  $slide4_alt = check_plain(theme_get_setting('slide4_alt'));

  $show_slide5 = theme_get_setting('show_slide5');
  $slide5_head = check_plain(theme_get_setting('slide5_head'));
  $slide5_desc = filter_xss_admin(theme_get_setting('slide5_desc'));
  $slide5_url = check_plain(theme_get_setting('slide5_url'));
  $slide5_img = check_plain(theme_get_setting('slide5_image_url'));
  $slide5_alt = check_plain(theme_get_setting('slide5_alt'));

  $show_slide6 = theme_get_setting('show_slide6');
  $slide6_head = check_plain(theme_get_setting('slide6_head'));
  $slide6_desc = filter_xss_admin(theme_get_setting('slide6_desc'));
  $slide6_url = check_plain(theme_get_setting('slide6_url'));
  $slide6_img = check_plain(theme_get_setting('slide6_image_url'));
  $slide6_alt = check_plain(theme_get_setting('slide6_alt'));

  $show_slide7 = theme_get_setting('show_slide7');
  $slide7_head = check_plain(theme_get_setting('slide7_head'));
  $slide7_desc = filter_xss_admin(theme_get_setting('slide7_desc'));
  $slide7_url = check_plain(theme_get_setting('slide7_url'));
  $slide7_img = check_plain(theme_get_setting('slide7_image_url'));
  $slide7_alt = check_plain(theme_get_setting('slide7_alt'));
?>

<div id="slider" role="complementary" <?php if (theme_get_setting('slidety') == '0'): ?>style="max-width: <?php print zeropoint_wrapper_width() ?>; margin: 0 auto;"<?php endif; ?>>
<ul class="slides">
  <?php if($show_slide1) { ?>
  <li>
    <div class="post slide1">
    <div class="entry-container">
    <div class="txww">
      <div class="entry-header"><div class="entry-title"><a href="<?php print url($slide1_url); ?>"><?php print $slide1_head; ?></a></div></div>
      <div class="entry-summary"><?php print $slide1_desc; ?></div>
    </div>
    </div>
    <a href="<?php print url($slide1_url); ?>">
    <img src="<?php print $slide1_img; ?>" class="slide-image" <?php if (theme_get_setting('slideh')): ?>style="max-height: <?php print zeropoint_slide_height() ?>"<?php endif; ?> alt="<?php print $slide1_alt; ?>" /></a>
    </div>
  </li>
  <?php } ?>
  <?php if($show_slide2) { ?>
  <li>
    <div class="post slide2">
    <div class="entry-container">
    <div class="txww">
      <div class="entry-header"><div class="entry-title"><a href="<?php print url($slide2_url); ?>"><?php print $slide2_head; ?></a></div></div>
      <div class="entry-summary"><?php print $slide2_desc; ?></div>
    </div>
    </div>
    <a href="<?php print url($slide2_url); ?>">
    <img src="<?php print $slide2_img; ?>" class="slide-image" <?php if (theme_get_setting('slideh')): ?>style="max-height: <?php print zeropoint_slide_height() ?>"<?php endif; ?> alt="<?php print $slide2_alt; ?>" /></a>
    </div>
  </li>
  <?php } ?>
  <?php if($show_slide3) { ?>
  <li>
    <div class="post slide3">
    <div class="entry-container">
    <div class="txww">
      <div class="entry-header"><div class="entry-title"><a href="<?php print url($slide3_url); ?>"><?php print $slide3_head; ?></a></div></div>
      <div class="entry-summary"><?php print $slide3_desc; ?></div>
    </div>
    </div>
    <a href="<?php print url($slide3_url); ?>">
    <img src="<?php print $slide3_img; ?>" class="slide-image" <?php if (theme_get_setting('slideh')): ?>style="max-height: <?php print zeropoint_slide_height() ?>"<?php endif; ?> alt="<?php print $slide3_alt; ?>" /></a>
    </div>
  </li>
  <?php } ?>
  <?php if($show_slide4) { ?>
  <li>
    <div class="post slide4">
    <div class="entry-container">
    <div class="txww">
      <div class="entry-header"><div class="entry-title"><a href="<?php print url($slide4_url); ?>"><?php print $slide4_head; ?></a></div></div>
      <div class="entry-summary"><?php print $slide4_desc; ?></div>
    </div>
    </div>
    <a href="<?php print url($slide4_url); ?>">
    <img src="<?php print $slide4_img; ?>" class="slide-image" <?php if (theme_get_setting('slideh')): ?>style="max-height: <?php print zeropoint_slide_height() ?>"<?php endif; ?> alt="<?php print $slide4_alt; ?>" /></a>
    </div>
  </li>
  <?php } ?>
  <?php if($show_slide5) { ?>
  <li>
    <div class="post slide5">
    <div class="entry-container">
    <div class="txww">
      <div class="entry-header"><div class="entry-title"><a href="<?php print url($slide5_url); ?>"><?php print $slide5_head; ?></a></div></div>
      <div class="entry-summary"><?php print $slide5_desc; ?></div>
    </div>
    </div>
    <a href="<?php print url($slide5_url); ?>">
    <img src="<?php print $slide5_img; ?>" class="slide-image" <?php if (theme_get_setting('slideh')): ?>style="max-height: <?php print zeropoint_slide_height() ?>"<?php endif; ?> alt="<?php print $slide5_alt; ?>" /></a>
    </div>
  </li>
  <?php } ?>
  <?php if($show_slide6) { ?>
  <li>
    <div class="post slide6">
    <div class="entry-container">
    <div class="txww">
      <div class="entry-header"><div class="entry-title"><a href="<?php print url($slide6_url); ?>"><?php print $slide6_head; ?></a></div></div>
      <div class="entry-summary"><?php print $slide6_desc; ?></div>
    </div>
    </div>
    <a href="<?php print url($slide6_url); ?>">
    <img src="<?php print $slide6_img; ?>" class="slide-image" <?php if (theme_get_setting('slideh')): ?>style="max-height: <?php print zeropoint_slide_height() ?>"<?php endif; ?> alt="<?php print $slide6_alt; ?>" /></a>
    </div>
  </li>
  <?php } ?>
  <?php if($show_slide7) { ?>
  <li>
    <div class="post slide7">
    <div class="entry-container">
    <div class="txww">
      <div class="entry-header"><div class="entry-title"><a href="<?php print url($slide7_url); ?>"><?php print $slide7_head; ?></a></div></div>
      <div class="entry-summary"><?php print $slide7_desc; ?></div>
    </div>
    </div>
    <a href="<?php print url($slide7_url); ?>">
    <img src="<?php print $slide7_img; ?>" class="slide-image" <?php if (theme_get_setting('slideh')): ?>style="max-height: <?php print zeropoint_slide_height() ?>"<?php endif; ?> alt="<?php print $slide7_alt; ?>" /></a>
    </div>
  </li>
  <?php } ?>
</ul>
</div>
